#!/bin/bash
set -e

IP=1.1.1.1

menu() {
    echo "1) Create a new repo"
    echo "2) Delete a repo"
    echo "3) Show all the repo stats"
    read -rp "Your choose: " CHOOSE
    case $CHOOSE in
        "1") create;;
        "2") del;;
        "3") stat;;
        *) echo "?";;
    esac
}
create() {
    read -rp "Name: " NAME
    read -rp "SSH Public Key: " SSHKEY
    USERNAME=$(< /dev/urandom tr -dc "[:lower:]" | head -c"${1:-8}";echo;)
    useradd -md /home/"$USERNAME" "$USERNAME"
    mkdir /home/"$USERNAME"/.ssh /home/"$USERNAME"/repo
    echo command=\"borg serve --restrict-to-repository /home/"$USERNAME"/repo --append-only\",restrict "$SSHKEY" > /home/"$USERNAME"/.ssh/authorized_keys
    chmod 0400 /home/"$USERNAME"/.ssh/authorized_keys
    echo "$NAME" > /home/"$USERNAME"/borg_info
    chown "$USERNAME":"$USERNAME" /home/"$USERNAME" -R
    echo borg init --encryption=repokey-blake2 "$USERNAME"@"$IP:repo
}
del() {
    read -rp "Name: " NAME
    userdel -rf "$NAME"
}
stat() {
    temp=$(mktemp)
    echo -e "User\tName\tUsage\tLastModifyTime" >> "$temp"
    cd /home
    for i in */borg_info
    do
        path=${i/borg_info/}
        user=$(echo "$path"|awk -F/ '{print $1}')
        disk=$(du -h --max-depth=0 "$path"|tail -1|awk '{print $1}')
        if [[ -f "$path/repo/transactions" ]]
        then
            lastmodify=$(tail -1 "$path"/repo/transactions|awk '{print $5}')
        else
            lastmodify=0
        fi
        echo -e "$user\t$(cat "$i")\t$disk\t$lastmodify" >> "$temp"
    done
    column -t "$temp"
    rm "$temp"
}

menu
