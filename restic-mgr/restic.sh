#!/bin/bash
set -e

IP=1.1.1.1

menu() {
    echo "1) Create a new repo"
    echo "2) Delete a repo"
    echo "3) Show all the repo stats"
    echo "4) Change repo password"
    read -rp "Your choose: " CHOOSE
    case $CHOOSE in
        "1") create;;
        "2") del;;
        "3") stat;;
        "4") changepw;;
        *) echo "?";;
    esac
}
create() {
    read -rp "Name: " NAME
    USERNAME=$(< /dev/urandom tr -dc "[:lower:]" | head -c"${1:-8}";echo;)
    PASSWORD=$(< /dev/urandom tr -dc "0-9a-zA-Z" | head -c"${1:-16}";echo;)
    htpasswd -B -b /srv/restic/repo/.htpasswd "$USERNAME" "$PASSWORD"
    echo "$USERNAME" - "$NAME" >> /srv/restic/info
    echo restic -r rest:http://"$USERNAME":"$PASSWORD"@"$IP":1919/"$USERNAME"/ init
    kill -HUP $(pidof restic-rest-server)
}
del() {
    read -rp "Name: " NAME
    rm -r /srv/restic/repo/"$NAME"
    sed "/${NAME}:/d" -i /srv/restic/repo/.htpasswd
    sed "/${NAME} - /d" -i /srv/restic/info
    kill -HUP $(pidof restic-rest-server)
}
stat() {
    temp=$(mktemp)
    echo -e "User\tName\tUsage" >> "$temp"
    for path in /srv/restic/repo/*
    do
        user=${path/\/srv\/restic\/repo\//}
        name=$(fgrep "$user" /srv/restic/info|awk -F ' - ' '{print $2}')
        disk=$(du -h --max-depth=0 "$path"|tail -1|awk '{print $1}')
        echo -e "$user\t$name\t$disk" >> "$temp"
    done
    column -t "$temp"
    rm "$temp"
}
changepw() {
    read -rp "Username: " USERNAME
    read -rp "New Password: " PASSWORD
    htpasswd -B -b /srv/restic/repo/.htpasswd "$USERNAME" "$PASSWORD"
    kill -HUP $(pidof restic-rest-server)
}

menu
